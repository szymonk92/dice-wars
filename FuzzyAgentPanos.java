package agents;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import mainGame.DrawBoard;
import mainGame.Vertex;
import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;


public class FuzzyAgentPanos 
{
	private static final String MY_NUMBER_OF_DIECES = "me";
	private static final String DIFFERENCE = "difference";
	private static final String NUMBER_OF_MY_HEXES = "numberOfMyHexes";
	private static final String NUMBER_OF_ALL_HEXES = "numberOfAllHexes";
	private static final String NUMBER_OF_OPPONENT_HEXES = "numberOfOpponentHexes";
	private static final String SUM_OF_DICES_ON_NEIGHBOURS_OF_NEIGHBOUR_HEXES = "sumOfDicesOnNeighboursOfNeighbour";
	private static final String NEIGHBOUR1 = "neighbour1";
	private static final String NEIGHBOUR2 = "neighbour2";
	private static final String NEIGHBOUR3 = "neighbour3";
	private static final String NEIGHBOUR4 = "neighbour4";
	private static final String NEIGHBOUR5 = "neighbour5";
	
	private static final String RESULT = "result";

	private int capsule = 0;
	FIS fuzzyInterfaceSystem;
	
	List<Vertex> troopersAmountOfUnits = new ArrayList<Vertex>();
	List<Vertex> enemyAmountOfUnits = new ArrayList<Vertex>();
	List<Vertex> troopersNearEnemyAmountOfUnits = new ArrayList<Vertex>();
	HashMap<Vertex, List<Vertex>> trooperRadar = new HashMap<Vertex,List<Vertex>>();
	List<Vertex> enemyNeighBourUnits = new ArrayList<Vertex>();
	
	
	public FuzzyAgentPanos(String fileName) {
		fuzzyInterfaceSystem = FIS.load(fileName, true);
		if (fuzzyInterfaceSystem == null) {
			System.exit(1);
		}
	}

	public void chooseFuzzy(DrawBoard db, int player) {
		// Thread.currentThread().stop();

		System.out.println("Player: " + player);

		Vertex tempVertex = null;
		List<Vertex> tempList = null;
		List<Vertex> enemyTempList = null;
		troopersAmountOfUnits.clear();
		enemyAmountOfUnits.clear();
		
		for(int i = 0; i<db.LB.allHexes.size(); i++)
		{
			//Clear list each turn
			Vertex currentHex = null;
			
			currentHex = db.LB.allHexes.get(i);
			if(currentHex.getPlayer() == player)
			{
				troopersAmountOfUnits.add(currentHex);
			}else {
				enemyAmountOfUnits.add(currentHex);
			}
		}
				
		for(int i = 0; i<troopersAmountOfUnits.size();i++)
		{
			tempVertex = troopersAmountOfUnits.get(i);
			getEnemyNeighbours(tempVertex, db);;
		}

		for(int i = 0; i< troopersNearEnemyAmountOfUnits.size(); i++)
		{
			tempVertex = troopersNearEnemyAmountOfUnits.get(i);
			chooseHexToAttack(db, tempVertex);
		}

		db.LB.changePlayer();
		db.LB.isFinalState(db, player);
		/*if (capsule < tempList.size()) {
			tempVertex = tempList.get(capsule);
			db.randomClick(tempVertex.getX(), tempVertex.getY()); // highlighting
			System.out.println("Number of dices on current hex: "
					+ tempVertex.getNumberOfDices());
			System.out.println();
			chooseHexToAttack(db, tempVertex);

		} else {
			capsule = 0;
			db.LB.changePlayer();
			db.LB.isFinalState(db, player);
		}*/

	}
	
	public void getEnemyNeighbours(Vertex n, DrawBoard db)
	{		
		//Our trooper next to enemies
		if(n.getNeighbours().size()>1)
		{			
			for(int x =0; x<n.getNeighbours().size(); x++)
			{
				Vertex neighBourVertex = db.LB.board[n.getNeighbours().get(x).get(0)]
													[n.getNeighbours().get(x).get(1)];
				//Check if its enemy neighbour
				if(enemyAmountOfUnits.contains(neighBourVertex))
				{
					//If not on the List of neighbour enemies add it
					if(!enemyNeighBourUnits.contains(neighBourVertex))
					{
						enemyNeighBourUnits.add(neighBourVertex);
					}
					//Add the trooper to a special list of first liners
					if(!troopersNearEnemyAmountOfUnits.contains(n))
					{
						troopersNearEnemyAmountOfUnits.add(n);
						//Save in a hashMap
					}
				}
			}
			if(enemyNeighBourUnits.size()>0)
			{
				trooperRadar.put(n, enemyNeighBourUnits);
			}
		}
	}

	private void chooseHexToAttack(DrawBoard db, Vertex tempVertex) {

			int numberOfMyHexes = 0;
			int numberOfOpponentHexes = 0;
			int currentPlayer = tempVertex.getPlayer();

			if (currentPlayer == 1) 
			{
				numberOfMyHexes = troopersAmountOfUnits.size();
				numberOfOpponentHexes = enemyAmountOfUnits.size();
			} 

			List<Vertex> neighbours = new ArrayList<>();
			List<List<Integer>> neighboursOfMyNeighbour = new ArrayList<>();
			for (int i = 0; i < (tempVertex.getNeighbours().size() / 2); i++) {

				Vertex vert = db.LB.board[tempVertex.getNeighbours().get(i)
						.get(0)][tempVertex.getNeighbours().get(i).get(1)];
				neighbours.add(vert);
				System.out.println("Neighbour number of dices: "
						+ vert.getNumberOfDices());

				List<Integer> dicesOfNeighboursNeighbour = new ArrayList<>();

				for (int j = 0; j < vert.getNeighbours().size() / 2; j++) {
					Vertex vert2 = db.LB.board[vert.getNeighbours().get(j)
							.get(0)][vert.getNeighbours().get(j).get(1)];
					if (vert2.getPlayer() != currentPlayer) {
						dicesOfNeighboursNeighbour.add(vert2.getNumberOfDices()
								* (-1));
					} else {
						dicesOfNeighboursNeighbour
								.add(vert2.getNumberOfDices());
					}
				}
				dicesOfNeighboursNeighbour.remove(new Integer(tempVertex
						.getNumberOfDices()));
				neighboursOfMyNeighbour.add(dicesOfNeighboursNeighbour);

				System.out.println("Number of neighbours of the neighbour: "
						+ vert.getNeighbours().size() / 2);
				System.out.println("Neighbours of my nneighbour: "
						+ neighboursOfMyNeighbour.get(i));
				System.out.println();

			}
			System.out.println("I have " + neighbours.size() + " neighbours!");

			// Thread.currentThread().stop();

			List<Double> fuzzyResults = makeMagic(
					tempVertex.getNumberOfDices(), neighbours, numberOfMyHexes,
					numberOfOpponentHexes, neighboursOfMyNeighbour);

			attackHex(db, tempVertex, fuzzyResults);

			neighbours.clear();
			capsule++;
		
	}

	private List<Double> makeMagic(int numberOfDicesOnCurrentHex,
			List<Vertex> neighbours, int numberOfMyHexes,
			int numberOfOpponentHexes,
			List<List<Integer>> neighboursOfMyNeighbour) {

		List<Double> fuzzyResults = new ArrayList<>(0);
		for (int i = 0; i < neighbours.size(); i++) {

			FunctionBlock functionBlock = fuzzyInterfaceSystem
					.getFunctionBlock(null);

			// Set inputs
			functionBlock.setVariable(MY_NUMBER_OF_DIECES,
					numberOfDicesOnCurrentHex);

			functionBlock.setVariable(DIFFERENCE,
					(numberOfDicesOnCurrentHex - neighbours.get(i)
							.getNumberOfDices()));

			functionBlock.setVariable(NUMBER_OF_ALL_HEXES,
					(numberOfMyHexes + numberOfOpponentHexes));

			functionBlock.setVariable(NUMBER_OF_MY_HEXES, numberOfMyHexes);

			functionBlock.setVariable(NUMBER_OF_OPPONENT_HEXES,
					numberOfOpponentHexes);

			int sumOfDicesOnNeighboursOfNeighbourHexes = 0;
			for (int qwe : neighboursOfMyNeighbour.get(i)) {
				sumOfDicesOnNeighboursOfNeighbourHexes += qwe;
			}
			System.out.println(sumOfDicesOnNeighboursOfNeighbourHexes);
			// Thread.currentThread().stop();

			functionBlock.setVariable(
					SUM_OF_DICES_ON_NEIGHBOURS_OF_NEIGHBOUR_HEXES,
					sumOfDicesOnNeighboursOfNeighbourHexes);

			functionBlock.setVariable(NEIGHBOUR1,
					checkIfNotNull(neighboursOfMyNeighbour, i, 0));
			functionBlock.setVariable(NEIGHBOUR2,
					checkIfNotNull(neighboursOfMyNeighbour, i, 1));
			functionBlock.setVariable(NEIGHBOUR3,
					checkIfNotNull(neighboursOfMyNeighbour, i, 2));
			functionBlock.setVariable(NEIGHBOUR4,
					checkIfNotNull(neighboursOfMyNeighbour, i, 3));
			functionBlock.setVariable(NEIGHBOUR5,
					checkIfNotNull(neighboursOfMyNeighbour, i, 4));

			// Evaluate
			functionBlock.evaluate();
			functionBlock.getVariable(RESULT).defuzzify();
			fuzzyResults.add(functionBlock.getVariable(RESULT).getValue());
			System.out.println(functionBlock.getVariable(RESULT).getValue());
		}
		return fuzzyResults;
	}

	private int checkIfNotNull(List<List<Integer>> neighboursOfMyNeighbour,
			int i, int j) {
		
		if (i < neighboursOfMyNeighbour.get(i).size()) {
			if (j < neighboursOfMyNeighbour.get(i).size()) {
				return neighboursOfMyNeighbour.get(i).get(j);
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}

	private void attackHex(DrawBoard db, Vertex tempVertex,
			List<Double> fuzzyResults) {
		Double largetst = -10.0;

		for (int i = 0; i < fuzzyResults.size(); i++) {
			if (fuzzyResults.get(i) > largetst) {
				largetst = fuzzyResults.get(i);
			}
		}

		int index = fuzzyResults.indexOf(largetst);
		if (largetst > 0) {
			Vertex attackedVertex = db.LB.board[tempVertex.getNeighbours()
					.get(index).get(0)][tempVertex.getNeighbours().get(index)
					.get(1)];
			db.LB.attackHex(tempVertex, attackedVertex);
		}
	}


}
