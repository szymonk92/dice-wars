package mainGame;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JPanel;

public class DrawBoard extends JPanel {
	public LogicBoard LB = new LogicBoard();
	private ImageContainer IC = new ImageContainer();
	private Vertex vertex;
	private List<List<Integer>> neighbours;
	private int a, b;
	public int mode = 0; // 0 - select tile, 1 - attack

	public DrawBoard() {

		setFocusable(true);
		setBackground(Color.LIGHT_GRAY);
		setDoubleBuffered(true);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;

		selectHex(g2d);
		attack(g2d);

		g.dispose();
	}

	public void selectHex(Graphics2D g2d) {
		for (int i = 0; i < LogicBoard.NUMBER_OF_HEX_IN_AXIS_X; i++) {
			for (int j = 0; j < LogicBoard.NUMBER_OF_HEX_IN_AXIS_Y; j++) {
				vertex = LB.getVertex(i, j);
				if (j % 2 == 0) {
					drawHexes(g2d, 10, 32);
				} else {
					drawHexes(g2d, 54, 76);
				}
			}
		}
	}

	void drawHexes(Graphics2D g2d, int liczba1, int liczba2) {
		g2d.drawImage(vertex.getImage(), liczba1 + vertex.getX() * 88,
				25 + vertex.getY() * 25, this);
		if (vertex.getPlayer() != 0)
			g2d.drawString(Integer.toString(vertex.getNumberOfDices()), liczba2
					+ vertex.getX() * 88, 52 + vertex.getY() * 25);
	}

	void attack(Graphics2D g2d) {
		if (mode == 1) {
			for (List<Integer> neighbour : neighbours) {
				if (!neighbour.isEmpty()) {
					a = neighbour.get(0);
					b = neighbour.get(1);
					if (b % 2 == 0) {
						g2d.drawImage(IC.getAttack(), 10 + a * 88, 25 + b * 25,
								this);
					} else {
						g2d.drawImage(IC.getAttack(), 54 + a * 88, 25 + b * 25,
								this);
					}
				}
			}
		}
	}

	public void repaintNow(){
		repaint();
	}
	
}