package mainGame;

import javax.swing.JFrame;
import javax.swing.JButton;

import Clips.ClipsHandler;
import agents.BfsAgentPawel;
import agents.BfsAgentSzymon;
import agents.FuzzyAgent;
import agents.BfsAgentPanos;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.Timer;

public class MainWindow extends JFrame {

	static Timer timer;

	public static void main(String[] args) {

		timer = new Timer(10, new Update());
		timer.start();
	}

	DrawBoard DB = new DrawBoard();

	public BfsAgentPanos bfsAgent = new BfsAgentPanos();

	public MainWindow() {

		getContentPane().add(DB);
		DB.setLayout(null);

		JButton btnEndTurn = new JButton("End Turn");
		btnEndTurn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DB.LB.changePlayer();
				repaint();
			}
		});

		btnEndTurn.setBounds(12, 407, 97, 25);
		DB.add(btnEndTurn);

		JButton btnRestart = new JButton("Restart");
		btnRestart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DB.LB = new LogicBoard();

				DB.mode = 0;
				DB.repaint();
			}
		});
		btnRestart.setBounds(121, 407, 97, 25);
		DB.add(btnRestart);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(1);
			}
		});
		btnExit.setBounds(525, 407, 97, 25);
		DB.add(btnExit);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(640, 480);
		setLocationRelativeTo(null);
		setTitle("Dice Wars - ARMAGEDDON");
		setResizable(false);
		setVisible(true);

	}
}

class Update implements ActionListener {
	
	private static final String CLIPS_FILE_NAME = "base.clp";
	MainWindow main = new MainWindow();
	BfsAgentPanos bfsAgentPanos = new BfsAgentPanos();
	BfsAgentPawel bfsAgentPawel = new BfsAgentPawel();
	BfsAgentSzymon bfsAgentSzymon = new BfsAgentSzymon();
	FuzzyAgent fuzzyAgent = new FuzzyAgent("opponentSzymon.fcl");

	ClipsHandler clipsAgent = new ClipsHandler(CLIPS_FILE_NAME, main.DB.LB);

	private boolean start = false;
	int turnnumber = 0;

	public void actionPerformed(ActionEvent e) {
		if (!start) {
			// changing player to 1
			main.DB.LB.changePlayer();
			bfsAgentSzymon.init(main.DB, main.DB.LB.getPlayer());
			start = true;
		}
		main.DB.mode = 0;

		try {
			if (main.DB.LB.getPlayer() == 1) {// Yellow
								
				bfsAgentSzymon.bfsTroopers(main.DB, main.DB.LB.getPlayer());
				bfsAgentSzymon.BfsSearchHexes(main.DB, main.DB.LB.getPlayer());
				// bfsAgentSzymon.bfsTroopers(main.DB, main.DB.LB.getPlayer());
				// clipsAgent.PlayMe(main.DB, main.DB.LB.getPlayer());
				// fuzzyAgent.chooseFuzzy(main.DB, main.DB.LB.getPlayer());
			} else {// Blue
				fuzzyAgent.chooseFuzzy(main.DB, main.DB.LB.getPlayer());

			}
		} catch (Exception e1) {
			main.DB.LB.changePlayer();
			e1.printStackTrace();
		}

		if (main.DB.LB.isFinalState(main.DB, main.DB.LB.getPlayer())) {
			main.DB.LB = new LogicBoard();
			clipsAgent = new ClipsHandler(CLIPS_FILE_NAME, main.DB.LB);
			main.DB.mode = 0;
			main.DB.repaint();
		}

		main.DB.repaint();
	}
}
