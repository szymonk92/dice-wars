package mainGame;

import java.awt.Image;

import javax.swing.ImageIcon;


public class ImageContainer {
	private Image hex, hexYellow, hexBlue, attack, blank;
	private int height, width;
	
	
	public ImageContainer(){
		ImageIcon ii = new ImageIcon("images\\hex.png");
		hex = ii.getImage();
		ii = new ImageIcon("images\\hexYellow.png");
		hexYellow = ii.getImage();
		ii = new ImageIcon("images\\hexBlue.png");
		hexBlue = ii.getImage();
		ii = new ImageIcon("images\\attack.png");
		attack = ii.getImage();
		ii = new ImageIcon("images\\blank.png");
		blank = ii.getImage();
		height = ii.getIconHeight();
		width = ii.getIconWidth();
	}
	
	public Image getHex() {
		return hex;
	}
	public Image getHexBlue() {
		return hexBlue;
	}
	public Image getHexYellow() {
		return hexYellow;
	}
	public Image getAttack() {
		return attack;
	}
	public Image getBlank() {
		return blank;
	}
	public int getWidth(){
		return width;
	}
	public int getHeight(){
		return height;
	}
}
