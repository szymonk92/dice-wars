package mainGame;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

public class Vertex {

	private int x, y;
	private Image image;
	private int player; // 1 or 2
	private int numberOfDices;
	private List<List<Integer>> neighbours;
	public boolean visited = false;

	public Vertex() {
		neighbours = new ArrayList<>();
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public int getPlayer() {
		return player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}

	public int getNumberOfDices() {
		return numberOfDices;
	}

	public void setNumberOfDices(int numberOfDices) {
		this.numberOfDices = numberOfDices;
	}

	public List<List<Integer>> getNeighbours() {
		return neighbours;
	}

	public void setNeighbours(List<List<Integer>> neighbours) {
		this.neighbours = neighbours;
	}

	public void addNeighbour(int x, int y) {
		List<Integer> coordinates = new ArrayList<Integer>();
		coordinates.add(x);
		coordinates.add(y);
		neighbours.add(coordinates);
	}

	/*
	 * int getId(); int getPlayer(); int getNumberOfDices(); List<Integer>
	 * getNeighbours();
	 */
}
