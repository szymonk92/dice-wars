package mainGame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;

public class LogicBoard {
	static final int NUMBER_OF_HEX_IN_AXIS_X = 5; // displays always 2x more
	static final int NUMBER_OF_HEX_IN_AXIS_Y = 10;
	private static final int BLANK_HEX = 12; // 12 is optimal

	public Vertex[][] board = new Vertex[10][20];
	public List<Vertex> board1d = new ArrayList<Vertex>();
	public List<Vertex> yellowHexes = new ArrayList<Vertex>();
	public List<Vertex> blueHexes = new ArrayList<Vertex>();
	public List<Vertex> allHexes = new ArrayList<Vertex>();
	public ImageContainer imageContainer = new ImageContainer();
	private final Random random = new Random();
	private int tables[][] = {{1,1},{1,2},{1,4},{1,6},{2,2},{2,4},{2,1},{3,0},{3,1},{3,7},{3,8},{3,9},{4,1},{4,2},{4,3},{4,4},{4,7},{0,3},{0,1},{0,8},{0,4},{0,7},{4,5},{3,3},{4,9}};
	private int tables2[][] = {{2,2},{0,9},{4,1},{4,4},{0,3},{0,8},{3,6},{4,0},{4,8}};
	private int player = 2;
	private int[] playerNeighbourCounter = { 0, 0 };
	private int temp = 0;

	static final int MAX_NUMBER_OF_DIECES_ON_HEX = 8;
	int numberOfFields = 0;
	public Vertex[][] boardWithoutWhite;
	public boolean succesfullAttack = false;

	int dicesOnStart[] = { 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 4 };

//	LogicBoard() {
//		for (int i = 0; i < NUMBER_OF_HEX_IN_AXIS_X; i++) {
//			for (int j = 0; j < NUMBER_OF_HEX_IN_AXIS_Y; j++) {
//				setYellowImage(i, j);
//			}
//		}
//		shuffleHexes(); // divide hex
//		setBlankImage(); // set blank fields
//		setNeighbours();
//
//		checkIfIslandExists();
//		setNeighbours();
//		checkNeighbours(1);
//		changeToBoard1d();
//		// displayList();
//		// countAnyNeighbour();
//
//	}
	LogicBoard(){
		for (int i = 0; i < NUMBER_OF_HEX_IN_AXIS_X; i++) {
			for (int j = 0; j < NUMBER_OF_HEX_IN_AXIS_Y; j++) {
				setYellowImage(i, j);
			}
		}
		shuffleHexes2();
		setBlankImage2(); // set blank fields
		setNeighbours();

		checkIfIslandExists();
		setNeighbours();
		checkNeighbours(1);
		changeToBoard1d();
		
	}

	private void changeToBoard1d(){
		for(int x=0; x<NUMBER_OF_HEX_IN_AXIS_X; x++){
			for(int y=0; y<NUMBER_OF_HEX_IN_AXIS_Y; y++){
				if(board[x][y].getPlayer()!=0){
					board1d.add(board[x][y]);
				}
			}
		}
	}

	private void checkIfIslandExists() {
		for (int i = 0; i < NUMBER_OF_HEX_IN_AXIS_X; i++) {
			for (int j = 0; j < NUMBER_OF_HEX_IN_AXIS_Y; j++) {
				checkNeighbours(i, j);
			}
		}
	}

	private void checkNeighbours(int i, int j) {
		if (board[i][j].getImage() != imageContainer.getBlank()) {
			if (board[i][j].getNeighbours().isEmpty()) { // check if hex has
															// neighbours
				board[i][j].setPlayer(0);
				board[i][j].setImage(imageContainer.getBlank());
			} else if (board[i][j].getNeighbours().size() == 1) {
				List<List<Integer>> lists = board[i][j].getNeighbours();
				for (List<Integer> list : lists) {
					if (board[list.get(0)][list.get(1)].getNeighbours().size() == 1) {
						board[i][j].setPlayer(0);
						board[i][j].setImage(imageContainer.getBlank());
					}
				}
			}
		}
	}


	private static int player1 = 0;
	private static int player2 = 0;
	
	public boolean isFinalState(DrawBoard db, int player) {
		if (db.LB.countFields(player) == 0) {
			String playerName;
			if (player == 1) {
				playerName = "blue";
				player2 ++;
			} else {
				playerName = "yellow";
				player1 ++;
			}
			System.out.println("BLUE " + player2);
			System.out.println("YELLOW " + player1);
			
		//	if (player2+player1 == 10){
				JOptionPane.showMessageDialog(null, "Player " + playerName + " win!  :");
		//	}
			return true;
		} else {
			return false;
		}
	}

	private void setYellowImage(int i, int j) {
		int randomDices = new Random().nextInt(dicesOnStart.length);

		board[i][j] = new Vertex();
		board[i][j].setImage(imageContainer.getHexYellow());
		board[i][j].setPlayer(1);
		board[i][j].setX(i);
		board[i][j].setY(j);
		board[i][j].setNumberOfDices(dicesOnStart[randomDices]);
		// adding yellow hexes to list
		yellowHexes.add(board[i][j]);
		allHexes.add(board[i][j]);
		numberOfFields++;
	}

	public void setBlankImage() {
		for (int i = 0; i < BLANK_HEX; i++) {
			int a = random.nextInt(NUMBER_OF_HEX_IN_AXIS_X);
			int b = random.nextInt(NUMBER_OF_HEX_IN_AXIS_Y);
			if (board[a][b].getPlayer() != 0) {
				board[a][b].setPlayer(0);
				board[a][b].setImage(imageContainer.getBlank());
				board[a][b].setNumberOfDices(0);
				yellowHexes.remove(board[a][b]);
				blueHexes.remove(board[a][b]);
				allHexes.remove(board[a][b]);
				numberOfFields--;
			}
		}
	}
	public void setBlankImage2() {
		for (int i = 0; i < 9; i++) {
			int a = tables2[i][0];
			int b = tables2[i][1];
			if (board[a][b].getPlayer() != 0) {
				board[a][b].setPlayer(0);
				board[a][b].setImage(imageContainer.getBlank());
				board[a][b].setNumberOfDices(0);
				yellowHexes.remove(board[a][b]);
				blueHexes.remove(board[a][b]);
				allHexes.remove(board[a][b]);
				numberOfFields--;
			}
		}
	}

	private void shuffleHexes() {

		int size = (NUMBER_OF_HEX_IN_AXIS_X * NUMBER_OF_HEX_IN_AXIS_Y) / 2;

		for (int n = 0; n < size; n++) {
			int i = random.nextInt(NUMBER_OF_HEX_IN_AXIS_Y);
			int j = random.nextInt(NUMBER_OF_HEX_IN_AXIS_Y);
			if (board[i][j].getPlayer() == 1) {
				board[i][j].setImage(imageContainer.getHexBlue());
				board[i][j].setPlayer(2);
				// keeping only the real yellow ones
				yellowHexes.remove(board[i][j]);
				blueHexes.add(board[i][j]);
			} else
				n--;
		}
	}
	
	private void shuffleHexes2() {


		for (int n = 0; n < 25; n++) {
			int i = tables[n][0];
			int j = tables[n][1];
			if (board[i][j].getPlayer() == 1) {
				board[i][j].setImage(imageContainer.getHexBlue());
				board[i][j].setPlayer(2);
				// keeping only the real yellow ones
				yellowHexes.remove(board[i][j]);
				blueHexes.add(board[i][j]);
			} else
				n--;
		}
	}

	public Vertex getVertex(int x, int y) {
		return board[x][y];
	}

	public Boolean checkIfNeighbours(Vertex vertex1, Vertex vertex2) {

		for (int i = 0; i < vertex1.getNeighbours().size(); i++) {
			Vertex temp0 = board[vertex1.getNeighbours().get(i).get(0)][vertex1
					.getNeighbours().get(i).get(1)];
			if (temp0.getX() == vertex2.getX()
					&& temp0.getY() == vertex2.getY()) {
				return true;
			}
		}
		return false;
	}

	// First is our position and second is attacker.
	public Boolean attackHex(Vertex vertex1, Vertex vertex2) {
		if (checkIfNeighbours(vertex1, vertex2)) {
			// System.out.println("YES, THEY ARE NEIGHBOURS!");
			if (vertex1.getPlayer() != vertex2.getPlayer()) {
				if (vertex1.getNumberOfDices() > 1) {
					return checkDiceSum(vertex1, vertex2);
				}
			}
		}
		return false;
	}
	
	public Boolean attackBFSHex(Vertex vertex1, Vertex vertex2) {
		//if (checkIfNeighbours(vertex1, vertex2)) {
			// System.out.println("YES, THEY ARE NEIGHBOURS!");
			if (vertex1.getPlayer() != vertex2.getPlayer()) 
			{
				if (vertex1.getNumberOfDices() > 1) 
				{
					return checkDiceSum(vertex1, vertex2);
				}
			}
			return false;
		}

	private boolean checkDiceSum(Vertex vertex1, Vertex vertex2) {
		int vertex1Sum = 0, vertex2Sum = 0;
		vertex1Sum = randomDiecesWar(vertex1, vertex1Sum);
		vertex2Sum = randomDiecesWar(vertex2, vertex2Sum);
		// System.out.println("Player1: " + vertex1Sum + ", player2: "
		// + vertex2Sum);
		if (vertex1Sum == vertex2Sum || vertex1Sum < vertex2Sum) {
			vertex1.setNumberOfDices(1);
			return false;
		} else {
			vertex2.setNumberOfDices(vertex1.getNumberOfDices() - 1);
			vertex1.setNumberOfDices(1);
			vertex2.setPlayer(vertex1.getPlayer());
			vertex2.setImage(vertex1.getImage());
			return true;
		}
	}
	
//	private boolean checkDiceSumBFS(Vertex vertex1, Vertex vertex2) {
//		int vertex1Sum = 0, vertex2Sum = 0;
//		vertex1Sum = randomDiecesWar(vertex1, vertex1Sum);
//		vertex2Sum = randomDiecesWar(vertex2, vertex2Sum);
//		// System.out.println("Player1: " + vertex1Sum + ", player2: "
//		// + vertex2Sum);
//		if (vertex1Sum == vertex2Sum || vertex1Sum < vertex2Sum) {
//			vertex1.setNumberOfDices(1);
//			return false;
//		} else {
//			vertex2.setNumberOfDices(vertex1.getNumberOfDices() +1);
//			vertex1.setNumberOfDices(1);
//			vertex2.setPlayer(vertex1.getPlayer());
//			vertex2.setImage(vertex1.getImage());
//			if(vertex1.getPlayer() == 2)
//			{
//				System.out.println("Kaboum");
//			}
//			return true;
//		}
//	}
	

	private int randomDiecesWar(Vertex vertex2, int vertex2Sum) {
		for (int i = 0; i < vertex2.getNumberOfDices(); i++) {
			vertex2Sum += new Random().nextInt(6) + 1; // range [1,6]
			// System.out.println("number " + vertex2Sum);
		}
		return vertex2Sum;
	}

	// =========== Private functions for setting the Board

	private void setNeighbours() {

		for (int i = 0; i < NUMBER_OF_HEX_IN_AXIS_X; i++) {
			for (int j = 0; j < NUMBER_OF_HEX_IN_AXIS_Y; j++) {
				if (j % 2 == 0) {

					if (j > 0 && i > 0 && board[i - 1][j - 1].getPlayer() != 0)
						board[i][j].addNeighbour(i - 1, j - 1);
					if (i > 0 && board[i - 1][j + 1].getPlayer() != 0)
						board[i][j].addNeighbour(i - 1, j + 1);
					if (j > 0 && board[i][j - 2].getPlayer() != 0)
						board[i][j].addNeighbour(i, j - 2);
					if (j < LogicBoard.NUMBER_OF_HEX_IN_AXIS_Y - 2
							&& board[i][j + 2].getPlayer() != 0)
						board[i][j].addNeighbour(i, j + 2);
					if (i < LogicBoard.NUMBER_OF_HEX_IN_AXIS_X && j > 0
							&& board[i][j - 1].getPlayer() != 0)
						board[i][j].addNeighbour(i, j - 1);
					if (i < LogicBoard.NUMBER_OF_HEX_IN_AXIS_X
							&& board[i][j + 1].getPlayer() != 0)
						board[i][j].addNeighbour(i, j + 1);
				} else {
					if (j > 1 && board[i][j - 2].getPlayer() != 0)
						board[i][j].addNeighbour(i, j - 2);
					if (board[i][j - 1].getPlayer() != 0)
						board[i][j].addNeighbour(i, j - 1);
					if (j < LogicBoard.NUMBER_OF_HEX_IN_AXIS_Y - 1
							&& board[i][j + 1].getPlayer() != 0)
						board[i][j].addNeighbour(i, j + 1);
					if (j < LogicBoard.NUMBER_OF_HEX_IN_AXIS_Y - 1
							&& board[i][j + 2].getPlayer() != 0)
						board[i][j].addNeighbour(i, j + 2);
					if (i < LogicBoard.NUMBER_OF_HEX_IN_AXIS_X - 1
							&& board[i + 1][j - 1].getPlayer() != 0)
						board[i][j].addNeighbour(i + 1, j - 1);
					if (i < LogicBoard.NUMBER_OF_HEX_IN_AXIS_X - 1
							&& j < LogicBoard.NUMBER_OF_HEX_IN_AXIS_Y - 1
							&& board[i + 1][j + 1].getPlayer() != 0)
						board[i][j].addNeighbour(i + 1, j + 1);
				}
				numberOfFields--;
			}
		}

	}

	public int countFields(int currentPlayer) {
		int fieldCount = 0;
		for (int x = 0; x < NUMBER_OF_HEX_IN_AXIS_X; x++) {
			for (int y = 0; y < NUMBER_OF_HEX_IN_AXIS_Y; y++) {
				if (board[x][y].getPlayer() == currentPlayer)
					fieldCount++;
			}
		}
		return fieldCount;
	}

	void addDiecesAfterTurn(int player) {
		for (int x = 0; x < NUMBER_OF_HEX_IN_AXIS_X; x++) {
			for (int y = 0; y < NUMBER_OF_HEX_IN_AXIS_Y; y++) {
				//if(random.nextInt(2) == 1){
					if (board[x][y].getPlayer() == player
							&& board[x][y].getNumberOfDices() < MAX_NUMBER_OF_DIECES_ON_HEX) {
						board[x][y]
								.setNumberOfDices(board[x][y].getNumberOfDices() + 1);
					}
				//}
			}
		}
	}

	public void changePlayer() {
		if (player == 1) {
			// System.out.println("Pierwszy: " + countFields(player));
			addDiecesAfterTurn(player);
			player = 2;
		} else if (player == 2) {
			// System.out.println("Drugi: " + countFields(player));
			addDiecesAfterTurn(player);
			player = 1;
		}
	}

	public int getPlayer() {
		return player;
	}

	// ============
	// == sth. there is responsible for checking the size of your current field
	private void checkNeighbours(int player) {
		for (int x = 0; x < NUMBER_OF_HEX_IN_AXIS_X; x++) {
			for (int y = 0; y < NUMBER_OF_HEX_IN_AXIS_Y; y++) {
				playerNeighbourCounter[player] = countNeighbours(player,
						board[x][y]);
				if (temp < playerNeighbourCounter[player])
					temp = playerNeighbourCounter[player];
			}
		}


		for (int x = 0; x < NUMBER_OF_HEX_IN_AXIS_X; x++) {
			for (int y = 0; y < NUMBER_OF_HEX_IN_AXIS_Y; y++) {
				if (board[x][y].getPlayer() == 11) {
					board[x][y].setPlayer(1);
				}
				if (board[x][y].getPlayer() == 22) {
					board[x][y].setPlayer(2);
				}
			}
		}
	}

	private int countNeighbours(int player, Vertex hex) {
		if (hex.getPlayer() != player)
			return 0;
		hex.setPlayer(player * 11);
		playerNeighbourCounter[player]++;
		for (int i = 0; i < hex.getNeighbours().size(); i++) {
			if (board[hex.getNeighbours().get(i).get(0)][hex.getNeighbours()
					.get(i).get(1)].getPlayer() == player) {
				playerNeighbourCounter[player] = countNeighbours(player,
						board[hex.getNeighbours().get(i).get(0)][hex
								.getNeighbours().get(i).get(1)]);
			}
		}
		return playerNeighbourCounter[player];
	}

}
