package Clips;

import mainGame.DrawBoard;
import mainGame.LogicBoard;
import mainGame.Vertex;
import CLIPSJNI.*;

public class ClipsHandler {
	
	Environment clips= new Environment();
	ClipsBoard clipsBoard = new ClipsBoard();

	
	public ClipsHandler(String agent, LogicBoard LB){
		
		clipsBoard.SetBaseFacts(clips, agent, LB);
		clips.reset();
		
	}
	
	public void PlayMe(DrawBoard db, int player) throws Exception{

		clips.reset();
		clipsBoard.SetBoard(clips, db.LB);
		clips.run();
		
		while(true){
			clips.reset();
			clipsBoard.SetBoard(clips, db.LB);
			clips.run();
			
			PrimitiveValue pv = clips.eval(ClipsBoard.getNextMoveFact());
			if(pv.size() >= 1){
				PrimitiveValue tv = pv.get(pv.size()-1);
				if(tv.getFactSlot("endTurn").symbolValue().compareToIgnoreCase("true") == 0){
					System.out.println("Turn Finished");
					break;
				}
				Attack(tv,db.LB);
			//	System.out.println("from " + tv.getFactSlot("from") + " to " + tv.getFactSlot("to"));
			}
			else if(pv.size() > 1){
				System.out.println("ERROR: To many moves");

				break;
			}
			else{
				System.out.println("ERROR: No move to do");
				break;
			}
		}
		
//		Thread.sleep(10000);
		db.LB.changePlayer();
		db.LB.isFinalState(db, player);
		
//		clips.eval("(facts)");
		
	}
	private void Attack(PrimitiveValue tv, LogicBoard LB) throws Exception{
		Vertex vertex1 = LB.board1d.get(tv.getFactSlot("from").intValue());
		Vertex vertex2 = LB.board1d.get(tv.getFactSlot("to").intValue());
		LB.attackHex(vertex1, vertex2);
	}

}
