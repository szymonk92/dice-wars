package Clips;

import mainGame.LogicBoard;
import CLIPSJNI.Environment;

public class ClipsBoard {
	private int ruleindex = 0;
	
	
	public static String getNextMoveFact() {
		return "(find-all-facts ((?v next-move))(eq 1 1))";
	}
	
	public void SetBaseFacts(Environment clips, String BaseName, LogicBoard LB){
		clips.load(BaseName);
		SetNeighbours(clips, LB);
		clips.reset();
	}
	
	public void SetBoard(Environment clips, LogicBoard LB){
		clips.build("(defglobal ?*player* = " + LB.getPlayer() + ")");
		clips.eval("(assert (next-move (from 1) (to 1) (endTurn true)))");
		for(int i=0; i < LB.board1d.size(); i++){
			clips.eval("(assert (hex(id " + i + ")(dices " + LB.board1d.get(i).getNumberOfDices() + ")(playerNumber " + LB.board1d.get(i).getPlayer() + ")))");
		}
	}
	
	private void SetNeighbours(Environment clips, LogicBoard LB){
		int x,y;
		for(int i=0; i < LB.board1d.size(); i++){
			for(int j=0; j < LB.board1d.get(i).getNeighbours().size(); j++){
				x = LB.board1d.get(i).getNeighbours().get(j).get(0);
				y = LB.board1d.get(i).getNeighbours().get(j).get(1);
				for(int n=0; n<LB.board1d.size(); n++){
					if(x == LB.board1d.get(n).getX()){
						if(y == LB.board1d.get(n).getY()){
							SetHexHex(clips,i,n);
						}
					}
				}
			}
		}
	}
	private void SetHexHex(Environment clips, int id1, int id2){
		clips.build("(deffacts h-h"+ruleindex+" (hex-hex(from " + id1 + ")(to +" + id2 + ")))");
		ruleindex++;
	}
}
