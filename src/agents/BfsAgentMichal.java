package agents;

import java.util.List;

import mainGame.DrawBoard;
import mainGame.Vertex;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import BFS.Node;

public class BfsAgentMichal
{
	
	public Vertex rootNode;
	public List<Vertex> childrenNodes = new ArrayList<Vertex>();
	int size;
	int myNumberOfDices;
	
	public void setRootNode(Vertex n)
	{
		this.rootNode = n;
	}
	
	public Vertex getRootNode()
	{
		return this.rootNode;
	}
	
	private Vertex getUnvisitedChildNode(Vertex n)
	{
		int indes = childrenNodes.indexOf(n);
		int j = 0;
		
		while(j<size)
		{
			if(childrenNodes.get(j).visited == false)
			{
				return childrenNodes.get(j);
			}
			j++;
		}
		return null;
	}
	
	private void clearNodes()
	{
		int i=0;
		while(i<size)
		{
			Vertex n=(Vertex)childrenNodes.get(i);
			n.visited=false;
			i++;
		}
	}

	private int index = 0;
	
	public void BfsSearchHexes(DrawBoard db, int player)
	{
		Queue q = new LinkedList();
		Vertex tempVertex = null;
		List<Vertex> tempList = null;
		Vertex attackedVertexRoot = null;
		
		
		if (player == 1) 
		{
			tempList = db.LB.yellowHexes;
		} else
		{
			tempList = db.LB.blueHexes;
		}
		
	
		if(index < tempList.size())
		{
			//Get our position and number of dices
			
			tempVertex = tempList.get(index);
			myNumberOfDices = tempVertex.getNumberOfDices();
			
			size = tempVertex.getNeighbours().size();
			this.rootNode = tempVertex;
			rootNode.visited = true;
			q.add(rootNode);
			
			//Add neighbours to our graphsearch
			for (int i = 0; i < tempVertex.getNeighbours().size(); i++) 
			{
					Vertex attackedVertex = db.LB.board[tempVertex
							.getNeighbours().get(i).get(0)][tempVertex
							.getNeighbours().get(i).get(1)];
					
					//Connect them
					childrenNodes.add(attackedVertex);	
			}
			
			////////////////////////////////////////BFS our neighbours and attack the one with the smallest number of dices OR equal
			int smallestNumberOfDices = 8;
			Vertex enemyToBeAttacked = null;
			
			while(!q.isEmpty())
			{
				Vertex n = (Vertex)q.remove();
				Vertex child = null;
				
				while((child = getUnvisitedChildNode(n)) != null)
				{
					child.visited =true;
					q.add(child);
					if(child.getNumberOfDices()+2 <= smallestNumberOfDices)
					{
						enemyToBeAttacked = child;
						smallestNumberOfDices = child.getNumberOfDices();					
					}
				}		
			}
			
			//Attack time
			if(enemyToBeAttacked != null)
			{
				db.LB.attackHex(tempVertex, enemyToBeAttacked);
			//	System.out.println("Attack");
			}else {
				index = 0;
				db.LB.changePlayer();
				db.LB.isFinalState(db, player);
			}
			
			
			//Clears childerns list and sets them back to false for them to be checked again
			clearNodes();
			childrenNodes.clear();
			smallestNumberOfDices = 10;
			enemyToBeAttacked = null;
			index++ ;			
		} else 
		{
			index = 0;
			db.LB.changePlayer();
			db.LB.isFinalState(db, player);
		}
		
	}

}


