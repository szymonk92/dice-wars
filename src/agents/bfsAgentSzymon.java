/*
	Szymon Klepacz
 */

package agents;

import java.util.List;

import mainGame.DrawBoard;
import mainGame.Vertex;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Collections;

public class BfsAgentSzymon {

	public Vertex rootNode;
	public List<Vertex> childrenNodes = new ArrayList<Vertex>();
	public int[][] conections;
	int index = 0;
	int size;
	int myNumberOfDices;
	boolean dominatedEnemy = false;

	List<Vertex> troopersAmountOfUnits = new ArrayList<Vertex>();
	List<Vertex> troopersNearEnemyAmountOfUnits = new ArrayList<Vertex>();
	List<Vertex> enemyNeighBourUnits = new ArrayList<Vertex>();
	List<Vertex> enemyNextToEnemy = new ArrayList<Vertex>();
	List<Vertex> enemyAmountOfUnits = new ArrayList<Vertex>();

	HashMap<Vertex, List<Vertex>> trooperRadar = new HashMap<Vertex, List<Vertex>>();

	public void setRootNode(Vertex n) {
		this.rootNode = n;
	}

	public Vertex getRootNode() {
		return this.rootNode;
	}

	private Vertex getUnvisitedChildNode(Vertex n, DrawBoard db) {
		int target = db.LB.allHexes.indexOf(n);
		int j = 0;

		while (j < size) {
			if (conections[target][j] == 1
					&& db.LB.allHexes.get(j).visited == false) {
				return db.LB.allHexes.get(j);
			}
			j++;
		}
		return null;
	}

	private void clearNodes(List<Vertex> clearList) {
		int i = 0;
		while (i < size) {
			Vertex n = (Vertex) clearList.get(i);
			n.visited = false;
			i++;
		}
	}

	public void connectNode(Vertex start, Vertex end, DrawBoard db) {
		if (conections == null) {
			size = db.LB.allHexes.size();
			conections = new int[size][size];
		}

		int startIndex = db.LB.allHexes.indexOf(start);
		int endIndex = db.LB.allHexes.indexOf(end);

		conections[startIndex][endIndex] = 1;
		conections[endIndex][startIndex] = 1;

	}

	public void BfsSearchHexes(DrawBoard db, int player) {
		Queue<Vertex> q = new LinkedList<>();
		Vertex tempVertex = null;
		List<Vertex> tempList = null;

		tempList = player == 1 ? db.LB.yellowHexes : db.LB.blueHexes;

		for (int x = 0; x < tempList.size(); x++) {
			// Get our position and number of dices
			tempVertex = tempList.get(x);
			myNumberOfDices = tempVertex.getNumberOfDices();

			size = tempVertex.getNeighbours().size();
			size = size / 2;
			this.rootNode = tempVertex;
			rootNode.visited = true;
			q.add(rootNode);

			// Add neighbours to our graphsearch
			for (int i = 0; i < tempVertex.getNeighbours().size() / 2; i++) {
				Vertex attackedVertex = db.LB.board[tempVertex.getNeighbours()
						.get(i).get(0)][tempVertex.getNeighbours().get(i)
						.get(1)];

				// Connect them
				childrenNodes.add(attackedVertex);
			}

			// BFS our neighbours and attack the one with the smallest number of
			// dices OR equal
			int smallestNumberOfDices = 6;
			Vertex enemyToBeAttacked = null;

			while (!q.isEmpty()) {
				Vertex n = (Vertex) q.remove();
				Vertex child = null;

				while ((child = getUnvisitedChildNode(n, db)) != null) {
					child.visited = true;
					q.add(child);
					if (child.getNumberOfDices() <= smallestNumberOfDices) {
						enemyToBeAttacked = child;
						smallestNumberOfDices = child.getNumberOfDices();
					}
				}
			}

			// Attack time
			if (enemyToBeAttacked != null) {
				db.LB.attackHex(tempVertex, enemyToBeAttacked);
				// System.out.println("Attack");
			} else {
				return;
			}

			// Clears childerns list and sets them back to false for them to be
			// checked again
			// clearNodes();
			childrenNodes.clear();
			enemyToBeAttacked = null;
		}
		db.LB.changePlayer();
		db.LB.isFinalState(db, player);
	}

	public void init(DrawBoard db, int player) {
		// Inteligence of Troopers
		// Our Troopers and enemy troopers
		/*
		 * if (player == 1) { troopersAmountOfUnits = db.LB.yellowHexes;
		 * enemyAmountOfUnits = db.LB.blueHexes;
		 * 
		 * System.out.println(troopersAmountOfUnits.size()+"VS"+enemyAmountOfUnits
		 * .size()); } else { troopersAmountOfUnits = db.LB.blueHexes;
		 * enemyAmountOfUnits = db.LB.yellowHexes;
		 * System.out.println(troopersAmountOfUnits
		 * .size()+"VS"+enemyAmountOfUnits.size()); }
		 */
		troopersAmountOfUnits.clear();
		enemyAmountOfUnits.clear();

		for (int i = 0; i < db.LB.allHexes.size(); i++) {
			// Clear list each turn
			Vertex currentHex = null;

			currentHex = db.LB.allHexes.get(i);
			if (currentHex.getPlayer() == player) {
				troopersAmountOfUnits.add(currentHex);
			} else {
				enemyAmountOfUnits.add(currentHex);
			}
		}

		System.out.println(troopersAmountOfUnits.size()
				+ " Troopers VS Enemies " + enemyAmountOfUnits.size());
	}

	public void bfsTroopers(DrawBoard db, int player) {
		Vertex trooperVertex = null;
		Vertex topDogTrooper = null;

		// Our trooper next to enemies
		for (int i = 0; i < troopersAmountOfUnits.size(); i++) {
			trooperVertex = troopersAmountOfUnits.get(i);
			// ConnectThem
			getEnemyNeighbours(trooperVertex, db);
		}

		Collections.sort(troopersNearEnemyAmountOfUnits,
				new Comparator<Vertex>() {
					public int compare(Vertex v1, Vertex v2) {
						return v2.getNumberOfDices() > v1.getNumberOfDices() ? 0
								: -1;
					}
				});

		for (int i = 0; i < troopersNearEnemyAmountOfUnits.size(); i++) {

			topDogTrooper = troopersNearEnemyAmountOfUnits.get(i);
			rootNode = topDogTrooper;
			// Get enemies of top trooper
			enemyNeighBourUnits = trooperRadar.get(topDogTrooper);

			if (!enemyNeighBourUnits.isEmpty()) {
				for (int j = 0; j < enemyNeighBourUnits.size(); j++) {
					// Connect enemy neighbours
					connectNode(topDogTrooper, enemyNeighBourUnits.get(j), db);

					// Connect enemies of enemies
					if (topDogTrooper.getNumberOfDices() >= 6) {
						System.out.println("Expand Attack");
						getEnemyNextToEnemy(enemyNeighBourUnits.get(j), db);
						if (enemyNextToEnemy.size() > j + 1) {
							connectNode(enemyNeighBourUnits.get(j),
									enemyNextToEnemy.get(j), db);
						}
					}
					bfsAttack(db);

				}
				// bfsAttack(db);
			}
			// bfsAttack(db);
		}

		enemyNeighBourUnits.clear();
		enemyNextToEnemy.clear();
		troopersNearEnemyAmountOfUnits.clear();
		clearNodes(db.LB.allHexes);
		db.LB.changePlayer();
		db.LB.isFinalState(db, player);
	}

	public void getEnemyNeighbours(Vertex n, DrawBoard db) {

		// Our trooper next to enemies
		if (n.getNeighbours().size() > 1) {
			for (int x = 0; x < n.getNeighbours().size(); x++) {
				Vertex neighBourVertex = db.LB.board[n.getNeighbours().get(x)
						.get(0)][n.getNeighbours().get(x).get(1)];
				// Check if its enemy neighbour
				if (enemyAmountOfUnits.contains(neighBourVertex)) {
					// If not on the List of neighbour enemies add it
					if (!enemyNeighBourUnits.contains(neighBourVertex)) {
						enemyNeighBourUnits.add(neighBourVertex);
					}
					// Add the trooper to a special list of first liners
					if (!troopersNearEnemyAmountOfUnits.contains(n)) {
						troopersNearEnemyAmountOfUnits.add(n);
						// Save in a hashMap
					}
				}
			}
			if (enemyNeighBourUnits.size() > 0) {
				trooperRadar.put(n, enemyNeighBourUnits);
			}
		}
	}

	public void getEnemyNextToEnemy(Vertex n, DrawBoard db) {
		// Our trooper next to enemies
		if (n.getNeighbours().size() > 1) {
			for (int x = 0; x < n.getNeighbours().size(); x++) {
				Vertex neighBourVertex = db.LB.board[n.getNeighbours().get(x)
						.get(0)][n.getNeighbours().get(x).get(1)];
				// Check if its enemy neighbour
				if (enemyAmountOfUnits.contains(neighBourVertex)) {
					// If not on the List of neighbour enemies add it
					if (!enemyNextToEnemy.contains(neighBourVertex)) {
						enemyNextToEnemy.add(neighBourVertex);
					}
				}
			}
		}
	}

	public void getEnemyNeighboursV2(Vertex n, DrawBoard db) {
		List<Vertex> tempEnemyNeighbours = new ArrayList<Vertex>();
		// Our trooper next to enemies
		if (n.getNeighbours().size() > 1) {
			for (int x = 0; x < n.getNeighbours().size(); x++) {
				Vertex neighBourVertex = db.LB.board[n.getNeighbours().get(x)
						.get(0)][n.getNeighbours().get(x).get(1)];
				// Check if its enemy neighbour
				if (enemyAmountOfUnits.contains(neighBourVertex)) {
					// If not on the List of neighbour enemies add it
					if (!tempEnemyNeighbours.contains(neighBourVertex)) {
						tempEnemyNeighbours.add(neighBourVertex);
					}
					// Add the trooper to a special list of first liners
					if (!troopersNearEnemyAmountOfUnits.contains(n)) {
						troopersNearEnemyAmountOfUnits.add(n);
						// Save in a hashMap
					}
				}
			}
			if (tempEnemyNeighbours.size() > 0) {
				trooperRadar.put(n, tempEnemyNeighbours);
			}
		}
	}

	public Vertex summonTopDog() {
		// Pick out tropper with bigger dices
		int maxTrooperDices = 0;
		Vertex topDogTrooper = null;
		Vertex trooperVertex = null;
		for (int i = 0; i < troopersNearEnemyAmountOfUnits.size(); i++) {
			trooperVertex = troopersNearEnemyAmountOfUnits.get(i);
			if (maxTrooperDices <= trooperVertex.getNumberOfDices()) {
				maxTrooperDices = trooperVertex.getNumberOfDices();
				topDogTrooper = trooperVertex;
			}
		}
		return topDogTrooper;
	}

	public Vertex getWeakestEnemy() {
		int minEnemyDice = 10;
		Vertex enemySmashing = null;
		Vertex trooperVertex = null;
		for (int i = 0; i < enemyNeighBourUnits.size(); i++) {
			trooperVertex = enemyNeighBourUnits.get(i);
			if (minEnemyDice >= trooperVertex.getNumberOfDices() - 1) {
				minEnemyDice = trooperVertex.getNumberOfDices();
				enemySmashing = trooperVertex;
			}
		}
		return enemySmashing;
	}

	public void bfsAttack(DrawBoard db) {
		Queue<Vertex> q = new LinkedList<>();
		q.add(this.rootNode);
		rootNode.visited = true;

		while (!q.isEmpty()) {
			Vertex n = (Vertex) q.remove();
			Vertex child = null;
			while ((child = getUnvisitedChildNode(n, db)) != null) {
				child.visited = true;
				q.add(child);
				if (rootNode.getNumberOfDices() >= child.getNumberOfDices()) {
					dominatedEnemy = db.LB.attackBFSHex(rootNode, child);
				}
			}
		}
		clearNodes(db.LB.allHexes);
	}
}
