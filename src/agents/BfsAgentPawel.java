package agents;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import mainGame.DrawBoard;
import mainGame.Vertex;

public class BfsAgentPawel
{
	
	public Vertex rootNode;
	public List<Vertex> childrenNodes = new ArrayList<Vertex>();
	public int[][] conections;
	int index = 0;
	int size;
	int myNumberOfDices;
	boolean dominatedEnemy = false;
	//Troopers

	List<Vertex> troopersAmountOfUnits = new ArrayList<Vertex>();
	List<Vertex> troopersNearEnemyAmountOfUnits = new ArrayList<Vertex>();
	List<Vertex> enemyNeighBourUnits = new ArrayList<Vertex>();
	List<Vertex> enemyNextToEnemy = new ArrayList<Vertex>();
	List<Vertex> enemyAmountOfUnits = new ArrayList<Vertex>();
	
	HashMap<Vertex, List<Vertex>> trooperRadar = new HashMap<Vertex,List<Vertex>>();
	
	public void setRootNode(Vertex n)
	{
		this.rootNode = n;
	}
	
	public Vertex getRootNode()
	{
		return this.rootNode;
	}
	
	private Vertex getUnvisitedChildNode(Vertex n, DrawBoard db)
	{
		int target = db.LB.allHexes.indexOf(n);
		int j = 0;
		
		while(j<size)
		{
			if(conections[target][j] == 1 && db.LB.allHexes.get(j).visited == false)
			{
				return  db.LB.allHexes.get(j);
			}
			j++;
		}
		return null;
	}
	
	private void clearNodes(List<Vertex> clearList)
	{
		int i=0;
		while(i<size)
		{
			Vertex n=(Vertex)clearList.get(i);
			n.visited=false;
			i++;
		}
	}

	public void connectNode(Vertex start, Vertex end, DrawBoard db)
	{
		if(conections == null)
		{
			size = db.LB.allHexes.size();
			conections = new int[size][size];
		}
		
		int startIndex = db.LB.allHexes.indexOf(start);
		int endIndex = db.LB.allHexes.indexOf(end);
		
		conections[startIndex][endIndex] = 1;
		conections[endIndex][startIndex] = 1;

		System.out.println("new connection");
	
	}

	
	public void init(DrawBoard db, int player) 
	{
		
		troopersAmountOfUnits.clear();
		enemyAmountOfUnits.clear();

		for(int i = 0; i<db.LB.allHexes.size(); i++)
		{
			//Clear list each turn
			Vertex currentHex = null;
			
			currentHex = db.LB.allHexes.get(i);
			if(currentHex.getPlayer() == player)
			{
				troopersAmountOfUnits.add(currentHex);
			}else {
				enemyAmountOfUnits.add(currentHex);
			}
		}
		
		System.out.println(troopersAmountOfUnits.size()+" Troopers VS Enemies "+enemyAmountOfUnits.size());
	}

	//===============================
	//===============================
	//===============================
	//===============================
	//===============================
	
	@SuppressWarnings("null")
	public void bfsTroopers(DrawBoard db, int player) 
	{
		Vertex trooperVertex = null;
		Vertex topDogTrooper = null;
		Vertex weakestEnemy = null;
		
		for(int i = 0; i<troopersAmountOfUnits.size(); i++)
		{
			trooperVertex = troopersAmountOfUnits.get(i);
			getEnemyNeighbours(trooperVertex, db);
		}
		
		for(int i = 0; i< troopersNearEnemyAmountOfUnits.size(); i++)
		{
			topDogTrooper = troopersNearEnemyAmountOfUnits.get(i);
			rootNode = topDogTrooper;
		    enemyNeighBourUnits = trooperRadar.get(topDogTrooper);
		    for (int j = 0; j < enemyNeighBourUnits.size(); j++) 
	    	{
	    		//Connect enemy neighbours
				connectNode(topDogTrooper, enemyNeighBourUnits.get(j), db);	
//				if(topDogTrooper.getNumberOfDices()==8)
//				{
//					System.out.println("Expand Attack");
//					getEnemyNextToEnemy(enemyNeighBourUnits.get(j), db);
					connectNode(enemyNeighBourUnits.get(j), enemyNextToEnemy.get(j), db);
//				}
//				System.out.println("ATTACK");
				bfsAttack(db);
		    }
		}
		
		enemyNeighBourUnits.clear();
		enemyNextToEnemy.clear();
		troopersNearEnemyAmountOfUnits.clear();
		clearNodes(db.LB.allHexes);
		db.LB.changePlayer();
		db.LB.isFinalState(db, player);
	}
	
	public void getEnemyNeighbours(Vertex n, DrawBoard db)
	{
		
		//Our trooper next to enemies
		if(n.getNeighbours().size()>1)
		{			
			for(int x =0; x<n.getNeighbours().size(); x++)
			{
				Vertex neighBourVertex = db.LB.board[n.getNeighbours().get(x).get(0)]
													[n.getNeighbours().get(x).get(1)];
				//Check if its enemy neighbour
				if(enemyAmountOfUnits.contains(neighBourVertex))
				{
					//If not on the List of neighbour enemies add it
					if(!enemyNeighBourUnits.contains(neighBourVertex))
					{
						enemyNeighBourUnits.add(neighBourVertex);
					}
					//Add the trooper to a special list of first liners
					if(!troopersNearEnemyAmountOfUnits.contains(n))
					{
						troopersNearEnemyAmountOfUnits.add(n);
						//Save in a hashMap
					}
				}
			}
			if(enemyNeighBourUnits.size()>0)
			{
				trooperRadar.put(n, enemyNeighBourUnits);
			}
			
			
		}
	}
	
	public void getEnemyNextToEnemy(Vertex n, DrawBoard db)
	{	
		//Our trooper next to enemies
		if(n.getNeighbours().size()>1)
		{			
			for(int x =0; x<n.getNeighbours().size(); x++)
			{
				Vertex neighBourVertex = db.LB.board[n.getNeighbours().get(x).get(0)]
													[n.getNeighbours().get(x).get(1)];
				//Check if its enemy neighbour
				if(enemyAmountOfUnits.contains(neighBourVertex))
				{
					//If not on the List of neighbour enemies add it
					if(!enemyNextToEnemy.contains(neighBourVertex))
					{
						enemyNextToEnemy.add(neighBourVertex);
					}
				}
			}			
		}
	}


	
	public void bfsAttack(DrawBoard db)
	{
		Queue q = new LinkedList();
		q.add(this.rootNode);
		rootNode.visited = true;
		
		while (!q.isEmpty()) 
		{
			Vertex n = (Vertex)q.remove();
			Vertex child = null;
			while ((child = getUnvisitedChildNode(n, db)) != null) 
			{
				child.visited = true;
				q.add(child);
				//System.out.println(child.getNumberOfDices() + "Enemy jojojo Trooper"+rootNode.getNumberOfDices());
				if(rootNode.getNumberOfDices()>=child.getNumberOfDices())
				{
					dominatedEnemy = db.LB.attackBFSHex(rootNode, child);
				}
			}
		}
		clearNodes(db.LB.allHexes);
	}
}



