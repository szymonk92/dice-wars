package agents;

import java.util.ArrayList;
import java.util.List;

import mainGame.DrawBoard;
import mainGame.Vertex;
import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;

public class FuzzyAgent {

	/*
	 * input
	 */
	private static final String MY_NUMBER_OF_DIECES = "me";
	private static final String DIFFERENCE = "difference";
	private static final String NUMBER_OF_MY_HEXES = "numberOfMyHexes";
	private static final String NUMBER_OF_ALL_HEXES = "numberOfAllHexes";
	private static final String NUMBER_OF_OPPONENT_HEXES = "numberOfOpponentHexes";
	private static final String SUM_OF_DICES_ON_NEIGHBOURS_OF_NEIGHBOUR_HEXES = "sumOfDicesOnNeighboursOfNeighbour";
	private static final String NEIGHBOUR1 = "neighbour1";
	private static final String NEIGHBOUR2 = "neighbour2";
	private static final String NEIGHBOUR3 = "neighbour3";
	private static final String NEIGHBOUR4 = "neighbour4";
	private static final String NEIGHBOUR5 = "neighbour5";

	/*
	 * output
	 */
	private static final String RESULT = "result";
	private int capsule = 0;

	private FIS fuzzyInterfaceSystem;

	public FuzzyAgent(String fileName) {
		fuzzyInterfaceSystem = FIS.load(fileName, true);
		if (fuzzyInterfaceSystem == null) {
			System.exit(1);
		}
	}

	public void chooseFuzzy(DrawBoard db, int player) {

		System.out.println("Player: " + player);
		Vertex tempVertex = null;
		List<Vertex> tempList = new ArrayList<Vertex>();
		List<Vertex> allHexes = db.LB.board1d;
		for (int i = 0; i < allHexes.size(); i++) {
			if (allHexes.get(i).getPlayer() == player) {
				tempList.add((allHexes.get(i)));
			}
		}

		if (capsule < tempList.size()) {
			tempVertex = tempList.get(capsule);
			chooseHexToAttack(db, tempVertex);

		} else {
			capsule = 0;
			db.LB.changePlayer();
			db.LB.isFinalState(db, player);
		}
	}

	private void chooseHexToAttack(DrawBoard db, Vertex tempVertex) {

		int currentPlayer = tempVertex.getPlayer();

		if (currentPlayer == 1 || currentPlayer == 2) {
			int numberOfMyHexes;
			int numberOfOpponentHexes;
			int tmpSize = 0;

			List<Vertex> allHexes = db.LB.board1d;

			for (int i = 0; i < allHexes.size(); i++) {
				if (allHexes.get(i).getPlayer() == currentPlayer) {
					tmpSize++;
				}
			}

			if (currentPlayer == 1) { // yellow
				numberOfMyHexes = tmpSize;
				System.out.println("Yellow hexes: " + tmpSize);
				numberOfOpponentHexes = allHexes.size() - tmpSize;
				System.out
						.println("Blue hexes: " + (allHexes.size() - tmpSize));

			} else {
				numberOfMyHexes = allHexes.size() - tmpSize;
				System.out.println("Yellow hexes: "
						+ (allHexes.size() - tmpSize));
				numberOfOpponentHexes = tmpSize;
				System.out.println("Blue hexes: " + tmpSize);
			}

			List<Vertex> neighbours = new ArrayList<>();
			List<List<Integer>> neighboursOfMyNeighbour = new ArrayList<>();
			for (int i = 0; i < (tempVertex.getNeighbours().size() / 2); i++) {

				Vertex vert = db.LB.board[tempVertex.getNeighbours().get(i)
						.get(0)][tempVertex.getNeighbours().get(i).get(1)];
				neighbours.add(vert);

				System.out.println("Neighbour number of dices: "
						+ vert.getNumberOfDices());

				List<Integer> dicesOfNeighboursNeighbour = new ArrayList<>();

				for (int j = 0; j < vert.getNeighbours().size() / 2; j++) {
					Vertex vert2 = db.LB.board[vert.getNeighbours().get(j)
							.get(0)][vert.getNeighbours().get(j).get(1)];
					if (vert2.getPlayer() != currentPlayer) {
						dicesOfNeighboursNeighbour.add(vert2.getNumberOfDices()
								* (-1));
					} else {
						dicesOfNeighboursNeighbour
								.add(vert2.getNumberOfDices());
					}
				}
				dicesOfNeighboursNeighbour.remove(new Integer(tempVertex
						.getNumberOfDices()));
				neighboursOfMyNeighbour.add(dicesOfNeighboursNeighbour);

				System.out.println("Number of neighbours of the neighbour: "
						+ vert.getNeighbours().size() / 2);
				System.out.println("Neighbours of my neighbour: "
						+ neighboursOfMyNeighbour.get(i));
				System.out.println();

			}
			System.out.println("I have " + neighbours.size() + " neighbours!");

			List<Double> fuzzyResults = setInputs(
					tempVertex.getNumberOfDices(), neighbours, numberOfMyHexes,
					numberOfOpponentHexes, neighboursOfMyNeighbour,
					currentPlayer);

			attackHex(db, tempVertex, fuzzyResults, currentPlayer);

			neighbours.clear();
			capsule++;
		}
	}

	private List<Double> setInputs(int numberOfDicesOnCurrentHex,
			List<Vertex> neighbours, int numberOfMyHexes,
			int numberOfOpponentHexes,
			List<List<Integer>> neighboursOfMyNeighbour, int currentPlayer) {

		List<Double> fuzzyResults = new ArrayList<>(0);
		for (int i = 0; i < neighbours.size(); i++) {

			// Get default function block
			FunctionBlock functionBlock = fuzzyInterfaceSystem
					.getFunctionBlock(null);

			// Set inputs
			functionBlock.setVariable(MY_NUMBER_OF_DIECES,
					numberOfDicesOnCurrentHex);

			int neighbourOrigin = neighbours.get(i).getPlayer();
			int differenceToSet = 0;
			if (neighbourOrigin == currentPlayer) {
				differenceToSet = -10;
			} else {
				differenceToSet = neighbours.get(i).getNumberOfDices() * (-1);
			}

			functionBlock.setVariable(DIFFERENCE,
					(numberOfDicesOnCurrentHex + differenceToSet));

			functionBlock.setVariable(NUMBER_OF_ALL_HEXES,
					(numberOfMyHexes + numberOfOpponentHexes));

			functionBlock.setVariable(NUMBER_OF_MY_HEXES, numberOfMyHexes);

			functionBlock.setVariable(NUMBER_OF_OPPONENT_HEXES,
					numberOfOpponentHexes);

			int sumOfDicesOnNeighboursOfNeighbourHexes = 0;

			for (int numberOfDices : neighboursOfMyNeighbour.get(i)) {
				sumOfDicesOnNeighboursOfNeighbourHexes += numberOfDices;
			}

			functionBlock.setVariable(
					SUM_OF_DICES_ON_NEIGHBOURS_OF_NEIGHBOUR_HEXES,
					sumOfDicesOnNeighboursOfNeighbourHexes);

			functionBlock.setVariable(NEIGHBOUR1,
					checkIfNotNull(neighboursOfMyNeighbour, i, 0));
			functionBlock.setVariable(NEIGHBOUR2,
					checkIfNotNull(neighboursOfMyNeighbour, i, 1));
			functionBlock.setVariable(NEIGHBOUR3,
					checkIfNotNull(neighboursOfMyNeighbour, i, 2));
			functionBlock.setVariable(NEIGHBOUR4,
					checkIfNotNull(neighboursOfMyNeighbour, i, 3));
			functionBlock.setVariable(NEIGHBOUR5,
					checkIfNotNull(neighboursOfMyNeighbour, i, 4));

			// Evaluate
			functionBlock.evaluate();
			functionBlock.getVariable(RESULT).defuzzify();
			fuzzyResults.add(functionBlock.getVariable(RESULT).getValue());
			// System.out.println(functionBlock.getVariable(RESULT).getValue());
		}
		return fuzzyResults;
	}

	private int checkIfNotNull(List<List<Integer>> neighboursOfMyNeighbour,
			int i, int j) {

		if (i < neighboursOfMyNeighbour.get(i).size()
				&& j < neighboursOfMyNeighbour.get(i).size()) {

			return neighboursOfMyNeighbour.get(i).get(j);

		} else {
			return 0;
		}
	}

	private void attackHex(DrawBoard db, Vertex tempVertex,
			List<Double> fuzzyResults, int currentPlayer) {
		Double largetst = 0.;

		for (int i = 0; i < fuzzyResults.size(); i++) {
			if (fuzzyResults.get(i) > largetst) {
				largetst = fuzzyResults.get(i);
			}
		}

		int index = fuzzyResults.indexOf(largetst);
		if (largetst > 0) {
			Vertex attackedVertex = db.LB.board[tempVertex.getNeighbours()
					.get(index).get(0)][tempVertex.getNeighbours().get(index)
					.get(1)];

			db.LB.attackHex(tempVertex, attackedVertex);
			db.repaintNow();

		}
	}

}
