; 
; Pawel Ciazynski 171120
;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Templates:

(deftemplate hex-hex
   (slot from)
   (slot to))
   
(deftemplate hex
   (slot id)
   (slot dices)
   (slot playerNumber))
   
(deftemplate next-move
   (slot from)
   (slot to)
   (slot endTurn     
    (type SYMBOL)
    (allowed-symbols true false)
    (default false)))

(deftemplate can-attack
    (slot from)
	(slot to))	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Global Rules:
(defglobal ?*player* = 1)
(defglobal ?*counter* = 0)

(defrule set-can-attack
	(declare (salience 100))
	
	(hex (id ?id) (dices ?dices) (playerNumber ?playerNumber))
	(hex-hex (from ?id) (to ?to))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))
	(test (> ?dices 1))
	(test (= ?playerNumber ?*player*))
	(not (test (= ?playerNumber ?playerNumber2)))
	
	=>
	(assert
		(can-attack
			(from ?id)
			(to ?to)
		)
	)
)

;(assert (next-move (from 1) (to 1)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Own Rules:



(defrule simple-move
	(declare (salience 90))
	
	(can-attack (from ?from) (to ?to))
	(hex-hex (from ?to) (to ?to2))
	(hex (id ?from) (dices ?dices1) (playerNumber ?playerNumber1))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))
	(test (>= ?dices1 ?dices2))
	(or
		(test (>= ?dices1 6))
		(test (< ?dices2 3))
	)
	
	=>
	
	(assert 
		(next-move 
			(from ?from)
			(to ?to)
			(endTurn false)
		)
	)
;	(printout t "from " ?from " with dices " ?dices1 " of player " ?playerNumber1 " , to " ?to " with dices " ?dices2 " of player " ?playerNumber2  crlf)
)

(defrule extended-move
	(declare (salience 70))
	
	(can-attack (from ?from) (to ?to))
	(hex-hex (from ?to) (to ?to2))
	(hex (id ?from) (dices ?dices1) (playerNumber ?playerNumber1))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))	
	(hex (id ?to2) (dices ?dices3) (playerNumber ?playerNumber3))
	(test (= ?playerNumber1 ?playerNumber3))
	(test (>= ?dices1 ?dices2))
	(or
		(test (>= ?dices1 6))
		(test (< ?dices2 3))
	)
	=>
	
	(assert 
		(next-move 
			(from ?from)
			(to ?to)
			(endTurn false)
		)
	)
	
	(bind ?*counter* (+ ?*counter* 1))
;	(printout t "from " ?from " with dices " ?dices1 " of player " ?playerNumber1 " , to " ?to " with dices " ?dices2 " of player " ?playerNumber2 ", next hex " ?to2 ", counter " ?*counter* crlf)
)


(defrule extended-move-remove
	(declare (salience 80))
	
	(can-attack (from ?from) (to ?to))
	(hex-hex (from ?to) (to ?to2))
	(hex (id ?from) (dices ?dices1) (playerNumber ?playerNumber1))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))	
	(hex (id ?to2) (dices ?dices3) (playerNumber ?playerNumber3))
	(not (test (= ?playerNumber1 ?playerNumber3)))
	(test (>= ?dices1 ?dices2))
	(or
		(test (>= ?dices1 6))
		(test (< ?dices2 4))
	)
	=>
	
	(assert 
		(next-move 
			(from ?from)
			(to ?to)
			(endTurn false)
		)
	)
	
	(bind ?*counter* (- ?*counter* 1))
;	(printout t "from " ?from " with dices " ?dices1 " of player " ?playerNumber1 " , to " ?to " with dices " ?dices2 " of player " ?playerNumber2 ", next hex " ?to2 ", counter " ?*counter* crlf)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Notes:
;
; - Check if there are more Your hexes, or enemy (quite easy imo.)
	
	
