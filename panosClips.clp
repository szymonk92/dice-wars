;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Templates:

(deftemplate hex-hex
   (slot from)
   (slot to))
   
(deftemplate hex
   (slot id)
   (slot dices)
   (slot playerNumber))
   
(deftemplate next-move
   (slot from)
   (slot to)
   (slot endTurn     
    (type SYMBOL)
    (allowed-symbols true false)
    (default false)))

(deftemplate can-attack
    (slot from)
	(slot to))	
	
(deftemplate can-attack-attack
    (slot from)
	(slot to))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Global Rules:
(defglobal ?*player* = 1)

(defrule set-can-attack
	(declare (salience 20))
	
	(hex (id ?id) (dices ?dices) (playerNumber ?playerNumber))
	(hex-hex (from ?id) (to ?to))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))
	(test (> ?dices 1))
	(test (= ?playerNumber ?*player*))
	(not (test (= ?playerNumber ?playerNumber2)))
	
	=>
	(assert
		(can-attack
			(from ?id)
			(to ?to)
		)
	)
)

;(assert (next-move (from 1) (to 1)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Own Rules:

(defrule simple-move
	(declare (salience 10))
	
	(can-attack (from ?from) (to ?to))
	(hex (id ?from) (dices ?dices1) (playerNumber ?playerNumber1))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))
	
	(and
	(test(> ?dice1 1))
	(test (>= (?dices1) ?dices2))
	)
	
	=>
	
       (assert
		(can-attack-attack
			(from ?dice1)
			(to ?dice2)
		)
	)
;	(printout t "from " ?from " with dices " ?dices1 " of player " ?playerNumber1 " , to " ?to " with dices " ?dices2 " of player " ?playerNumber2 crlf)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defrule simple-move-move
	(declare (salience 9))
	
	(can-attack-attack (from ?from) (to ?to))
	(hex (id ?from) (dices ?dices1) (playerNumber ?playerNumber1))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))	
	(hex (id ?id) (dices ?dices) (playerNumber ?playerNumber))
	(hex-hex (from ?id) (to ?to))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))
	(test (> ?dices 1))
	(test (= ?playerNumber ?*player*))
	(not (test (= ?playerNumber ?playerNumber2)))
	
	(or
		(test (>= (- ?dices1 1) ?dices2))
		(test (= ?dices 8))
		(test (= ?dices 7))
		)
	
	=>
	(assert 
		(next-move 
			(from ?from)
			(to ?to)
			(endTurn false)
		)
	)
;	(printout t "from " ?from " with dices " ?dices1 " of player " ?playerNumber1 " , to " ?to " with dices " ?dices2 " of player " ?playerNumber2 crlf)
)



