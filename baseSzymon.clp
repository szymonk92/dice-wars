(deftemplate hex-hex
   (slot from)
   (slot to))
   
(deftemplate hex
   (slot id)
   (slot dices)
   (slot playerNumber))
   
(deftemplate next-move
   (slot from)
   (slot to)
   (slot endTurn     
    (type SYMBOL)
    (allowed-symbols true false)
    (default false)))

(deftemplate can-attack
    (slot from)
	(slot to))	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Global Rules ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
(defglobal ?*player* = 1)

(defrule set-can-attack
	(declare (salience 30))
	
	(hex (id ?id) (dices ?dices) (playerNumber ?playerNumber))
	(hex-hex (from ?id) (to ?to))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))
	(test (> ?dices 1))
	(test (= ?playerNumber ?*player*)) 
	(not (test (= ?playerNumber ?playerNumber2))) 
	
	=>
	(assert 
		(can-attack
			(from ?id)
			(to ?to)
		)
	)
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Own Rules ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 

(defrule simple-move

	(declare (salience 10))
	
	(hex (id ?id) (dices ?dices) (playerNumber ?playerNumber))
	(hex-hex (from ?id) (to ?to))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))
	(test (> ?dices 1))
	(test (= ?playerNumber ?*player*))
	(not (test (= ?playerNumber ?playerNumber2)))
		
	=>
	(assert (next-move (from ?from)(to ?to)(endTurn false)))
)

;one enemy neighbour
(defrule neighbour-move

	(declare (salience 15))
	
	(can-attack (from ?from) (to ?to))
	(hex-hex (from ?to) (to ?to2))
	(hex-hex (from ?to2) (to ?to3))
	(hex (id ?from) (dices ?dices1) (playerNumber ?playerNumber1))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))
	(hex (id ?to2) (dices ?dices3) (playerNumber ?playerNumber3))
	(not (test (= ?to2 ?to3)))
	;(hex (id ?to3) (dices ?dices4) (playerNumber ?playerNumber4))
	(test (>= ?dices1 ?dices2))
	;(not (test (= ?playerNumber1 ?playerNumber4)))
		
	=>
	(assert (next-move (from ?from)(to ?to)(endTurn false)))
)

(defrule duo-move

	(declare (salience 20))
	
	(hex (id ?id) (dices ?dices) (playerNumber ?playerNumber))
	(hex-hex (from ?id) (to ?to))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))
	(test (> ?dices 1))
	(test (> ?dices ?dices2))
	(test (= ?playerNumber ?*player*))
	(not (test (= ?playerNumber ?playerNumber2)))
		
	=>
	(assert (next-move (from ?from)(to ?to)(endTurn false)))
)






	





