(deftemplate hex-hex
   (slot from)
   (slot to))
   
(deftemplate hex
   (slot id)
   (slot dices)
   (slot playerNumber))
   
(deftemplate next-move
   (slot from)
   (slot to)
   (slot endTurn     
    (type SYMBOL)
    (allowed-symbols true false)
    (default false)))

(deftemplate possible-move
    (slot from)
	(slot to))	

; Global Rules:
	

(defglobal ?*player* = 1)
	
; Now we define own rules:

(defrule simple-move
;	(declare (salience 10))
	
	(hex (id ?id) (dices ?dices) (playerNumber ?playerNumber))
	(hex-hex (from ?id) (to ?to))
	(hex (id ?to) (dices ?dices2) (playerNumber ?playerNumber2))
	(not (test (= ?playerNumber ?playerNumber2)))
;	(test (= ?playerNumber ?*player*))
	(test (> ?dices 3))
	(test (< ?dices2 7))
;	(test (>= ?dices ?dices2))
	=>
	(assert 
		(next-move 
			(from ?id)
			(to ?to)
			(endTurn false)
		)
	)
;	(printout t "from " ?id " with dices " ?dices " of player " ?playerNumber " , to " ?to " with dices " ?dices2 " of player " ?playerNumber2 crlf)
)






